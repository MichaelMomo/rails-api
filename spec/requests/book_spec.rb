require 'rails_helper'
require './spec/factory/book.rb'

describe 'Books API', type: :request do

  describe 'GET /book' do

    before do # This will run before all tests code
      FactoryBot.create(:book, title: '1984', author: 'George Orwell')
    end

    it 'return all books' do


      FactoryBot.create(:book, title: 'The time Machine', author: 'H.G Wells')

      get '/api/v1/books'

      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body).size).to eq(2)
    end
  end

  describe 'POST /books' do
    it 'create a new book' do
      expect{
        post '/api/v1/books', params: { book: { title: 'The Martian', author: 'Andy Weir'}}
      }.to change { Book.count }.from(0).to(1)

      expect(response).to have_http_status(:created)
    end
  end

  describe 'DELETE /books/:id' do

    let(:book) {FactoryBot.create(:book, title: '1984', author: 'George Orwell')}
    # When the test start, the book can be created first
    # let!(:book) {FactoryBot.create(:book, title: '1984', author: 'George Orwell')}

    it 'deletes the books' do

      delete "/api/v1/books/#{book.id}"

      expect(response).to have_http_status(:no_content)
    end
  end

end
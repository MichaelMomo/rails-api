class ApplicationController < ActionController::API

  # If we call destroy method anywhere in our application we will have this exception in case it failed
  rescue_from ActiveRecord::RecordNotDestroyed, with: :not_destroyed

  private

  def not_destroyed(e)
    # In case the record is not destroyed, let me access the record errors
    render json: { errors: e.record.errors }, status: :unprocessable_entity
  end
end

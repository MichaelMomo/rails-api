# DONGMO API

To create new api we use the following command line

    $ rails new <apiname> --api
     
 you can make a request easily from command line:
 
    $ curl http://localhost:3000/books     ------> GET
    
    curl --request POST http://localhost:3000/books   ----> POST
    
    curl --header "content-type: application/json" --request POST --data '{"author": "Anna Smith", "title": "The Hero"}' http://localhost:3000/books -v
    
    you can add -v for watch is going on
    
 Create a controller with specified action named 'index'
 
    $ rails g controller BooksController index
    
 Generate a model with properties 
 
    $ rails g model Book title:string author:string
    
 After creating the model, run the migration to create the corresponding table
 
    $ rails db:migrate
    
 From console, you can manually add new book in your database
 
    $ rails c
    $ Book.create!(author: 'Michael Dongmo', title: 'De l effort au confort')
    
 To find a particular routes for some model, use this:
 
    $ rails routes | findstr book  -----> Windows
    
    $ rails routes | grep book  -----> Linux
    
#### STATUS CODE

Each status code has one number associated:
    
    status: :created ------------------> 201
    status: :unprocessable_entity  ----> 422
    status: :Ok   ---------------------> 200

All rails status code are available on the following link
    http://www.railsstatuscodes.com/
    
#### VALIDATIONS

we can add some validation to our model on rails

     class Book < ApplicationRecord
       validates :author, presence: true, length: {minimum: 3}
       validates :title, presence: true, length: {minimum: 3}
     end

In this example the attributes title and author should be feel by user and can not be empty. The minimum length is 3.
For more about validation, follows rails doc in this link: https://guides.rubyonrails.org/active_record_validations.html

#### Exception Handling

https://apidock.com/rails/ActiveSupport/Rescuable/ClassMethods/rescue_from

#### Naming space and versioning
When you’re building an API for a project, it is highly recommended that you version your API (which is a form of contract
between you, the API provider, and the consumer). Why? Because when the API changes, either because you released new features
or changed the endpoints, any application using the API would run the risk of being broken until they updated their code.
This can cause some serious issues with your API clients.

The first option to do the versioning, is to store the version as part of the URL. So we’ll need to start in 
the routes.rb file in the config folder.
First, we’ll namespace the Api then we add another namespace called v1 (for version 1) to specify the version that we want 
to make available to public.

    #config/routes.rb
    
    namespace :api do
        namespace :v1 do
          resources :books, only: [:index, :create, :destroy]
        end
    end 
    
#### Tests With RSpec

first open your Gems file and then add 

    # rspec-rails is a testing framework for Rails 5+.
      gem 'rspec-rails', '~> 4.0', '>= 4.0.2'
    # factory_bot_rails provides integration between factory_bot and rails 5.0 or newer
      gem 'factory_bot_rails', '~> 6.1'
      
    $ bundle install to download and install in your app
    after run 
    $ rails generate rspec:install
    
After that, the spec folder will be generated and inside you have to implement all your logic for tests. 
    
    
    
https://www.youtube.com/watch?v=jcOp-R7hV5s&ab_channel=TomKadwill  ---> next
